﻿namespace Sparky.Test
{
    [TestClass]
    public class CalculatorMSTests
    {
        [TestMethod]
        public void AddNumbers_InputTwoNumbers_GetTwoNumbersAddition() 
        {
            //Arrange
            Calculator calculator = new();

            //Act
            var result = calculator.AddNumbers(10, 5);

            //Assert
            Assert.AreEqual(result, 15);
        }
    }
}