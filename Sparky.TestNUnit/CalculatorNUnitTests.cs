﻿using NUnit.Framework;

namespace Sparky.TestNUnit
{
    [TestFixture]
    public class CalculatorNUnitTests
    {
        public Calculator calc { get; set; }
        public Greeter greet { get; set; }

        [SetUp]
        public void SetUp()
        {
            calc = new Calculator();
            greet = new Greeter();
        }

        [Test]
        public void AddNumbers_InputTwoNumbers_GetTwoNumbersAddition() 
        {
            //Arrange
            //Calculator calc = new();

            //Act
            var result = calc.AddNumbers(15, 5);

            //Assert
            Assert.That(result, Is.EqualTo(20));
        }

        [Test]
        [TestCase(11)]
        [TestCase(13)]
        [TestCase(15)]
        public void IsOddChecker_InputOddNumber_ReturnTrue(int a)
        {
            //Arrange
            //Calculator calc = new();

            //Act
            var result = calc.IsOddNumber(a);

            //Assert
            Assert.That(result, Is.EqualTo(true));
            Assert.That(result, Is.True);
        }

        [Test]
        [TestCase(11, ExpectedResult = true)]
        [TestCase(10, ExpectedResult = false)]
        public bool IsOddChecker_InputOddNumber_ReturnTrueIfOdd(int a)
        {
            //Arrange
            //Calculator calc = new();

            return calc.IsOddNumber(a);
        }

        [Test]
        [TestCase(5.4, 10.5)]
        public void IsDoubleChecker_InputTwoDoubleNumbers_GetDoubleNumber(double a, double b)
        {
            //Arrange
            //Calculator calc = new();

            //Act
            var result = calc.AddNumbersDouble(a, b);

            //Assert
            Assert.That(result, Is.EqualTo(15.9));
        }

        [Test]
        public void CombineName_InputTwoNames_GetGreetingString()
        {
            //Arrange
            //Greeter greet = new();

            //Act
            var result = greet.GreetAndCombineNames("John", "Doe");

            //Assert
            Assert.That(result, Is.EqualTo("Hello, John Doe"));
            Assert.That(result, Does.StartWith("Hello"));
            Assert.That(result, Does.Contain(","));
        }

        [Test]
        public void CombineName_InputTwoNames_GetNullValue()
        {
            //Arrange
            //Greeter greet = new();

            //Act
            greet.GreetAndCombineNames("John", "Doe");

            //Assert
            Assert.That(greet.Greeting, Is.Null);
        }
    }
}
