﻿namespace Sparky
{
    public class Greeter
    {
        public string? Greeting { get; set; }

        public string GreetAndCombineNames(string name, string lastName) 
        { 
            return $"Hello, {name} {lastName}";
        }
    }
}
