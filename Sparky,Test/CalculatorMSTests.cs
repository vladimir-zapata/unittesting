﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sparky;

namespace Sparky_Test
{
    [TestClass]
    public class CalculatorMSTests
    {
        [TestMethod]
        public void AddNumbers_InputTwoInt_TwoNumbersAddition() 
        {
            //Arrange
            Calculator calculator = new();

            //Act
            var result = calculator.AddNumbers(5, 10);

            //Assert
            Assert.AreEqual(result, 15);
        }
    }
}
